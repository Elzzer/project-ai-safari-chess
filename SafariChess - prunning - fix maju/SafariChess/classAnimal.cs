﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafariChess
{
    public interface ICloneable<T>
    {
        T Clone();
    }
    class classAnimal : ICloneable<classAnimal>
    {
        public string player;
        public string tipe;
        public string status;
        public Image gambar;
        public int power;
        public int posisi;

        public classAnimal Clone()
        {
            return new classAnimal(player,tipe,status,power,posisi) { };
        }
        public classAnimal(string player, string tipe, string status, int power, int posisi) {
            this.player = player;
            this.tipe = tipe;
            this.status = status;
            this.power = power;
            this.posisi = posisi;
        }

        public classAnimal(int ctr, string player)
        {
            this.player = player;
            power = ctr + 1;
            status = "alive";
            if (ctr == 0) {
                if(player == "p1")gambar = Image.FromFile("piece/mouse_p1.JPG");
                else if (player == "p2") gambar = Image.FromFile("piece/mouse_p2.JPG");
                tipe = "rat";
                //if (player == "p1") posisi = 1;//posisi = 48;
                if (player == "p1") posisi = 48;
                else posisi = 14;
            }
            else if (ctr == 1)
            {
                if (player == "p1") gambar = Image.FromFile("piece/cat_p1.JPG");
                else if (player == "p2") gambar = Image.FromFile("piece/cat_p2.JPG");
                tipe = "cat";
                if (player == "p1") posisi = 50;
                else posisi = 12;
            }
            else if (ctr == 2)
            {
                if (player == "p1") gambar = Image.FromFile("piece/dog_p1.JPG");
                else if (player == "p2") gambar = Image.FromFile("piece/dog_p2.JPG");
                tipe = "dog";
                if (player == "p1") posisi = 54;
                //else posisi = 9;//posisi = 8;
                else posisi = 8;
            }
            else if (ctr == 3)
            {
                if (player == "p1") gambar = Image.FromFile("piece/wolf_p1.JPG");
                else if (player == "p2") gambar = Image.FromFile("piece/wolf_p2.JPG");
                tipe = "wolf";
                if (player == "p1") posisi = 44;
                else posisi = 18;
            }
            else if (ctr == 4)
            {
                if (player == "p1") gambar = Image.FromFile("piece/cheetah_p1.JPG");
                else if (player == "p2") gambar = Image.FromFile("piece/cheetah_p2.JPG");
                tipe = "cheetah";
                if (player == "p1") posisi = 46;
                else posisi = 16;
            }
            else if (ctr == 5)
            {
                if (player == "p1") gambar = Image.FromFile("piece/tiger_p1.JPG");
                else if (player == "p2") gambar = Image.FromFile("piece/tiger_p2.JPG");
                tipe = "tiger";
                //if (player == "p1") posisi = 43;
                if (player == "p1") posisi = 56;
                else posisi = 6;
            }
            else if (ctr == 6)
            {
                if (player == "p1") gambar = Image.FromFile("piece/lion_p1.JPG");
                else if (player == "p2") gambar = Image.FromFile("piece/lion_p2.JPG");
                tipe = "lion";
                if (player == "p1") posisi = 62;
                //else posisi = 15;//posisi = 0;
                else posisi = 0;
            }
            else if (ctr == 7)
            {
                if (player == "p1") gambar = Image.FromFile("piece/gajah_p1.JPG");
                else if (player == "p2") gambar = Image.FromFile("piece/gajah_p2.JPG");
                tipe = "elephant";
                //if (player == "p1") posisi = 49;
                if (player == "p1") posisi = 42;
                else posisi = 20;
            }
            gambar = new Bitmap(gambar, new Size(50, 50));
        }

        public Boolean possibleMove(int tujuan, List<classMap> listMap, List<classAnimal> listP1, List<classAnimal> listP2, int indexAnimalTurn) {
            List<classAnimal> listAnimal = new List<classAnimal>();
            //if (player == "p2") listAnimal = listP1;
            //else listAnimal = listP2;
            if (player == "p2" && listP1[0].player == "p2") listAnimal = listP2;
            else if (player == "p2" && listP1[0].player == "p1") listAnimal = listP1;
            else if (player == "p1" && listP1[0].player == "p2") listAnimal = listP1;
            else if (player == "p1" && listP1[0].player == "p1") listAnimal = listP2;


            Boolean boolReturn = false;
            if (tipe != "rat" && listMap[tujuan].tipe == "water") boolReturn = false;
            else if (tujuan == posisi - 7 || tujuan == posisi + 7) boolReturn = true;
            else if (tujuan == posisi + 1 && (posisi + 1) % 7 != 0) boolReturn = true;
            else if (tujuan == posisi - 1 && posisi % 7 != 0) boolReturn = true;
            else if (tipe == "lion" || tipe == "tiger")
            {
                if ((posisi == 21 || posisi == 28 || posisi == 35 || posisi == 24 || posisi == 31 || posisi == 38) && tujuan == posisi + 3) {
                    if (listMap[posisi + 1].indexAnimal == -1 && listMap[posisi + 2].indexAnimal == -1) boolReturn = true;
                    else boolReturn = false;
                }
                else if ((posisi == 27 || posisi == 34 || posisi == 41 || posisi == 24 || posisi == 31 || posisi == 38) && tujuan == posisi - 3) {
                    if (listMap[posisi - 1].indexAnimal == -1 && listMap[posisi - 2].indexAnimal == -1) boolReturn = true;
                    else boolReturn = false;
                }
                else if ((posisi == 15 || posisi == 16 || posisi == 18 || posisi == 19) && tujuan == posisi + 28) {
                    if (listMap[posisi + 7].indexAnimal == -1 && listMap[posisi + 14].indexAnimal == -1 && listMap[posisi + 21].indexAnimal == -1) boolReturn = true;
                    else boolReturn = false;
                }
                else if ((posisi == 43 || posisi == 44 || posisi == 46 || posisi == 47) && tujuan == posisi - 28) {
                    if (listMap[posisi - 7].indexAnimal == -1 && listMap[posisi - 14].indexAnimal == -1 && listMap[posisi - 21].indexAnimal == -1) boolReturn = true;
                    else boolReturn = false;
                }
            }

            if (boolReturn) {
                //gk boleh taruh base
                if (tujuan == 3 && player == "p2") boolReturn = false;
                else if (tujuan == 59 && player == "p1") boolReturn = false;
            }

            //cek animal teman atau lawan dan lebih lemah atau tidak
            if (listMap[tujuan].indexAnimal != -1 && boolReturn) {
                try
                {
                    //if (listAnimal[listMap[tujuan].indexAnimal].posisi == tujuan && listAnimal[listMap[tujuan].indexAnimal].power <= power)
                    //{
                    //    boolReturn = true;
                    //}
                    //else boolReturn = false;
                    if (listAnimal[listMap[tujuan].indexAnimal].posisi == tujuan)
                    {
                        if (tipe == "rat" && listAnimal[listMap[tujuan].indexAnimal].tipe == "elephant" && listMap[posisi].tipe != "water") boolReturn = true;
                        else if (tipe == "elephant" && listAnimal[listMap[tujuan].indexAnimal].tipe == "rat") boolReturn = false;
                        else if (listAnimal[listMap[tujuan].indexAnimal].power <= power) boolReturn = true;
                        else boolReturn = false;
                    }
                    else boolReturn = false;
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(listMap[tujuan].indexAnimal+"error");
                    throw;
                }
                
            }

            //if (boolReturn)
            //{
            //    listMap[posisi].indexAnimal = -1;
            //    listMap[tujuan].indexAnimal = indexAnimalTurn;
            //}

            return boolReturn;
        }

        public void moveAnimal(List<classMap> listMap, List<classAnimal> listOpponent, int tujuan, int indexAnimal) {
            if (listMap[tujuan].indexAnimal != -1)
            { //kalo ada lawan dan bisa dimakan
                if (listOpponent[listMap[tujuan].indexAnimal].posisi == tujuan)
                {
                    if(tipe == "rat" && listOpponent[listMap[tujuan].indexAnimal].tipe == "elephant") listOpponent[listMap[tujuan].indexAnimal].status = "dead";
                    else if (listOpponent[listMap[tujuan].indexAnimal].power <= power) listOpponent[listMap[tujuan].indexAnimal].status = "dead";
                }
            }

            listMap[posisi].indexAnimal = -1;
            listMap[tujuan].indexAnimal = indexAnimal;
            posisi = tujuan;

            //kalo kena shield jd lemah
            if (player == "p1")
            {
                if (posisi == 2 || posisi == 4 || posisi == 10) power = 0;
                else
                {
                    if (tipe == "rat") power = 1;
                    else if (tipe == "cat") power = 2;
                    else if (tipe == "dog") power = 3;
                    else if (tipe == "wolf") power = 4;
                    else if (tipe == "cheetah") power = 5;
                    else if (tipe == "tiger") power = 6;
                    else if (tipe == "lion") power = 7;
                    else if (tipe == "elephant") power = 8;
                }
            }
            else if (player == "p2") {
                if ((posisi == 52 || posisi == 58 || posisi == 60) && player == "p2") power = 0;
                else
                {
                    if (tipe == "rat") power = 1;
                    else if (tipe == "cat") power = 2;
                    else if (tipe == "dog") power = 3;
                    else if (tipe == "wolf") power = 4;
                    else if (tipe == "cheetah") power = 5;
                    else if (tipe == "tiger") power = 6;
                    else if (tipe == "lion") power = 7;
                    else if (tipe == "elephant") power = 8;
                }
            }
            
        }
    }

}
